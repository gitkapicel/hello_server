   /*
    ** server.c -- serwer używający gniazd strumieniowych
    */

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>
    #include <math.h>
    #include "testB.h"

    #define MYPORT 7000    // port, z którym będą się łączyli użytkownicy
    #define MAXDATASIZE 100
    #define BACKLOG 10     // jak dużo możę być oczekujących połączeń w kolejce

    void sigchld_handler(int s)
    {
        while(wait(NULL) > 0);
    }

    int main(void)
    {
        int sockfd, new_fd,info,por;  // nasłuchuj na sock_fd, nowe połaczenia na new_fd
        struct sockaddr_in my_addr;    // informacja o moim adresie
        struct sockaddr_in their_addr; // informacja o adresie osoby łączącej się
        int sin_size;
        struct sigaction sa;
        int yes=1;
        char buf[MAXDATASIZE];
        char buf1[MAXDATASIZE];
        time_t t;
        time(&t);
       

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {//socket() tworzy i otwiera plik specjalny typu gniazdo i zwraca jego deskryptor 0-protokol
            perror("socket");
            exit(1);
        }

        if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }
        
        my_addr.sin_family = AF_INET;         // host byte order 
        // Gniazdo IPv4 to gniazdo pomiędzy dwoma procesami, potencjalnie działającymi na dwóch maszynach używające obecnej wersji adresów IP. (TCP/IP)
        my_addr.sin_port = htons(MYPORT);     // short, network byte order
        my_addr.sin_addr.s_addr = INADDR_ANY; // uzupełnij moim adresem IP 
        //INADDR_ANY kazdy adres okreslony (0.0.0.0) 
        memset(&(my_addr.sin_zero), '\0', 8); // wyzeruj resztę struktury

        if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
        //bindujemy (wiążemy) do naszego gniazda sock, adres naszego serwera
            perror("bind");
            exit(1);
        }

        if (listen(sockfd, BACKLOG) == -1) {
            perror("listen");
            exit(1);
        }

        sa.sa_handler = sigchld_handler; // zbierz martwe procesy
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;
        if (sigaction(SIGCHLD, &sa, NULL) == -1) {
            perror("sigaction");
            exit(1);
        }

        while(1)  // głowna pętla accept()
        { 
          sin_size = sizeof(struct sockaddr_in);
          if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
          // //wpisanie danych klienta do struktury wskazywanej przez adres_klienta. sin_size, to  wielkość struktury sockaddr_in
             perror("accept");
             continue;
          }
          printf("server: got connection from %s\n",inet_ntoa(their_addr.sin_addr));
           if (!fork()) // to jest proces-dziecko
           { 
             // close(sockfd); // dziecko nie potrzebuje gniazda nasłuchującego
           
            int i;
            int liczby[3];
            while(1) //pentla dla stalej komunikacji z klientem
            { 
             printf("Czekam \n");
             if((info=  read( new_fd,buf,MAXDATASIZE-1))==-1){//odbieranie wiadomosci
              perror("odbieranie");
              exit(1);
             }
             buf[info] = '\0';
             printf("Received: %s \n",buf);


             if(strcmp( buf, "1024" )==0){  //warunki sprawdzajace otzymana wiadomosc
              if(write( new_fd, "kwadratowa\n", 14) == - 1 )
               perror( "wysylanie" );
              int i;
              int liczby[3];
                 
             for(i=0;i<3;i++)//pentla do wprowadzania danych rownania kwadratowego
             { 
               printf("Czekamm na zmienne \n");
               if((info=  read( new_fd,buf,MAXDATASIZE-1))==-1){//odbieranie wiadomosci
                perror("odbieranie");
                exit(1);
             }
               buf[info] = '\0';



   	       int liczba;
               liczba=atoi(buf);

	       liczby[i]=liczba;
   	// numer = atoi(napis);
          
               printf("Received: %d \n",liczba);
               if(i==2)
               {

                float delta,x1,x2,x0;
                char w1[MAXDATASIZE],w2[MAXDATASIZE];
                char text[20]="Miejsca zerowe to:";
                delta=(liczby[1]*liczby[1])-(4*liczby[0]*liczby[2]) ;
                printf("Delta:%f\n",delta);

 
                    


                if(delta==0)
                {
                 x0=-liczby[1]/(2*liczby[0]);
                 snprintf(w1, 12,"%f",x0);

                 strcat(w1,"\n");
                
                

                 if(write( new_fd, w1, 25 ) == - 1 )
                 perror( "wysylanie" );
                 
                
                }
                else if(delta>0)
                {
                  x1=(-liczby[1]-sqrt(delta))/2*liczby[0];
                  x2=(-liczby[1]+sqrt(delta))/2*liczby[0];
                  snprintf(w1, 12,"%f",x1);
                  snprintf(w2, 12,"%f",x2);
                  strcat(text,w1); 
                  strcat(text," i ");
                  strcat(text,w2);
                  strcat(text,"\n");
                   
                  if(write( new_fd, text, 45 ) == - 1 )
                   perror( "wysylanie" );    
                }
                else
                {
                  
                  if(write( new_fd, "Brak rozwiazania \n", 35 ) == - 1 )
                   perror( "wysylanie" );
                } 
              }
             }//koniec pentli for dla obliczen ktora wykonuje sie 3 razy 
             
            }else if(strcmp(buf,"koniec")==0){
              if(send( new_fd, "Cu\n", 4, 0) == - 1)
                 perror("wysylanie");
              break;//jesli klient wyslal koniec opuszamy pentle while
            }else if(strcmp( buf, "1023" )==0){  //warunki sprawdzajace otzymana wiadomosc
             if(write( new_fd, ctime(&t), 25 ) == - 1 )
               perror( "wysylanie" );
            
            }else if(strcmp( buf, "1020" )==0){  //warunki sprawdzajace otzymana wiadomosc
             if(write( new_fd, "konwersja\n", 25 ) == - 1 )
               perror( "wysylanie" );
             
              int liczbaaa;
              printf("Czekam na zmienne \n");
              if((info=  read( new_fd,&liczbaaa,sizeof(liczbaaa)))==-1){//odbieranie wiadomosci
                perror("odbieranie");
                exit(1);
              }
              printf("Received: %d \n",liczbaaa);
              
              //liczbaaa=atoi(buf);
              int litle=konw(liczbaaa);
         
              printf("little endian:%d\n",litle);
             
              liczbaaa=konw(litle);

              printf("Big endian:%d\n",liczbaaa);
              sprintf(buf1,"%d",liczbaaa) ;
             // printf("%d\n",liczbaaa);
              //printf("%s\n",buf1);

              //warunki sprawdzajace otzymana wiadomosc
              if(write( new_fd, &liczbaaa, sizeof(liczbaaa) ) == - 1 )
               perror( "wysylanie" );
             

            /* }else if(strcmp( buf, "201326592" )==0){ ////litle i big
              int liczbaaa;
              liczbaaa=atoi(buf);
              int litle=konw(liczbaaa);
         
              sprintf(buf1,"%d",litle) ;


 //warunki sprawdzajace otzymana wiadomosc
             if(write( new_fd, buf1, 25 ) == - 1 )
               perror( "wysylanie" );
            
            
            
            */
            }else
            {
              if(write( new_fd, "Brak polecenia\n", 16 ) == - 1 )
                 perror( "wysylanie" );
            }

         }//koniec pentli do stalej komunikacji  
                close(new_fd);
                exit(0);
        }
         close(new_fd);  // rodzic nie potrzebuje tego
       }//koniec pentli accept()

        return 0;
    } 