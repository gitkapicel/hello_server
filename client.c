 /*
    ** client.c -- klient używający gniazd strumieniowych
    */

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <netdb.h>
    #include <sys/types.h>
    #include <netinet/in.h>
    #include <sys/socket.h>
    #include <stdbool.h>  
    #include "testB.h"  

    #define PORT 7000 // port, z którym klient będzie się łączył

    #define MAXDATASIZE 100 // maksymalna ilość dancyh, jaką możemy otrzymać na raz

    int main(int argc, char *argv[])
    {
        int sockfd, numbytes,wysyl;  
        char buf[MAXDATASIZE];
        char buf2[MAXDATASIZE];
        char buf1[MAXDATASIZE];
        struct hostent *he;
        struct sockaddr_in their_addr; // informacja o adresie osoby łączącej się
        
        

        if (argc != 2) {
            fprintf(stderr,"usage: client hostname\n");
            exit(1);
        }

        if ((he=gethostbyname(argv[1])) == NULL) {  // pobierz informacje o hoście
            perror("gethostbyname");
            exit(1);
        }

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("socket");
            exit(1);
        }

        their_addr.sin_family = AF_INET;    // host byte order 
        their_addr.sin_port = htons(PORT);  // short, network byte order 
        their_addr.sin_addr = *((struct in_addr *)he->h_addr);
        memset(&(their_addr.sin_zero), '\0', 8);  // wyzeruj resztę struktury

        if (connect(sockfd, (struct sockaddr *)&their_addr,
                                              sizeof(struct sockaddr)) == -1) {
            perror("connect");
            exit(1);
        }
      for(;;){    //nieskonczona petla z ktorej wychodzimy po otrzymaniu odpowiedniego komunikatu
         printf("Co chcesz napisac:");
         scanf("%s",buf1);//wpisywanie z klawiatury




         /*int liczba;
         liczba=atoi(buf1);





         if(liczba==12)
         {
          int litle=konw(liczba);
         
         sprintf(buf1,"%d",litle) ;
         }*/
         if((wysyl =write(sockfd,buf1,MAXDATASIZE-1))==-1){
         perror("Wysylanie ");
           exit(1);
         }

         if ((numbytes=recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
            perror("recv");
            exit(1);
         }

         if(strcmp(buf,"konwersja\n")==0)
         {
           int liczba;
           printf("Podaj liczbe do konwersji:");
           
           scanf("%d",&liczba);//wpisywanie z klawiatury
           
           //liczba=atoi(buf1);
           int litle=konw(liczba);    
           //sprintf(buf1,"%d",litle) ;
           printf("Big endian:%d\n",litle);
          // printf("%s\n",buf1);           

           if((wysyl =write(sockfd,&litle,sizeof(litle)))==-1){
           perror("Wysylanie ");
           exit(1);
           }
            
         if((numbytes=  read( sockfd,&liczba,sizeof(liczba)))==-1){//odbieranie wiadomosci
                perror("odbieranie");
                exit(1);
              }
            
            //liczba=atoi(buf);
            litle=konw(liczba);
           
            sprintf(buf,"%d",litle) ;

         }

         if(strcmp(buf,"kwadratowa\n")==0)
         {
            
           printf("Podaj a:");
           scanf("%s",buf1);//wpisywanie z klawiatury
           if((wysyl =write(sockfd,buf1,MAXDATASIZE-1))==-1){
           perror("Wysylanie ");
           exit(1);
           }
            printf("Podaj b:");
           scanf("%s",buf1);//wpisywanie z klawiatury
           if((wysyl =write(sockfd,buf1,MAXDATASIZE-1))==-1){
           perror("Wysylanie ");
           exit(1);
           }
            printf("podaj c:");
           scanf("%s",buf1);//wpisywanie z klawiatury
           if((wysyl =write(sockfd,buf1,MAXDATASIZE-1))==-1){
           perror("Wysylanie ");
           exit(1);
           }
           if ((numbytes=recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
            perror("recv");
            exit(1);
           }

          
         /*  if ((numbytes=recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
            perror("recv");
            exit(1);
           }

           printf("Received: %s",buf);*/

         }
         if(strcmp(buf,"Cu\n")==0) break;//jesli otrzymalismy od servera wiadomosc Cu opuszczamy pentle
        
         buf[numbytes] = '\0';
           
         printf("Received: %s\n",buf);
        }
            
        buf[numbytes] = '\0';  //potrzebne w celu wyswietlenia w przypadku gdy opuscilismy pentle
        printf("Received: %s\n",buf);
        close(sockfd);

        return 0;
    } 